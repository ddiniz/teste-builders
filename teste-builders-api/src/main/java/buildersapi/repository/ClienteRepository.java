package buildersapi.repository;

import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import javax.ws.rs.core.Response.Status;

import buildersapi.dtos.ClienteDTO;
import buildersapi.dtos.FiltroClienteDTO;
import buildersapi.entities.Cliente;
import buildersapi.interfaces.repository.IClienteRepository;
import buildersapi.utils.exceptions.BuildersException;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

@ApplicationScoped
public class ClienteRepository implements IClienteRepository, PanacheRepository<Cliente> {

	@Override
	public Cliente exists(long id) throws BuildersException {
		Cliente result = findById(id);
		if (result == null)
			throw new BuildersException("Cliente com id (" + id + ") não foi encontrado.", Status.NOT_FOUND);
		return result;
	}

	@Override
	@Transactional
	public Cliente save(ClienteDTO cliente) throws BuildersException {
		Cliente novo = cliente.toEntity();
		persist(novo);
		return novo;
	}

	@Override
	@Transactional
	public Cliente update(ClienteDTO clienteUpdated) throws BuildersException {
		Cliente cliente = findById(clienteUpdated.getId());
		cliente.nome = clienteUpdated.getNome();
		cliente.idade = clienteUpdated.getIdade();
		cliente.endereco = clienteUpdated.getEndereco();
		cliente.cpf = clienteUpdated.getCpf();
		cliente.telefone = clienteUpdated.getTelefone();
		return cliente;
	}

	@Override
	@Transactional
	public boolean delete(long id) throws BuildersException {
		return deleteById(id);
	}

	@Override
	public List<Cliente> listar(int pagina, int qtdPerPage) throws BuildersException {
		PanacheQuery<Cliente> todosClientes = findAll();
		return todosClientes.page(pagina, qtdPerPage).list();
	}

	@Override
	public List<Cliente> filter(FiltroClienteDTO filtroCliente) {
		String query = filtroCliente.getQuery();
		Map<String, Object> params = filtroCliente.getParams();
		return find(query, params).list();
	}

}
