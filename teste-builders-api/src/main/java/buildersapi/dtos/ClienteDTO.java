package buildersapi.dtos;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.hibernate.validator.constraints.br.CPF;

import buildersapi.entities.Cliente;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ClienteDTO {
	public long id;
	@NotEmpty
	@Size(min = 1, max = 20, message = "Nome deve ser de 1 a 20 caracteres.")
	public String nome;
	@NotNull
	@Min(value = 1, message = "Idade mínima de 1 ano")
	@Max(value = 120, message = "Idade máxima de 120 anos")
	public Integer idade;
	@NotNull
	@Size(min = 1, max = 200, message = "Endereço deve ser de 1 a 20 caracteres.")
	public String endereco;
	@NotNull
	@CPF
	public String cpf;
	@NotNull
	@Pattern(message = "Telefone deve seguir o padrão '(XX) XXXXX-XXXX' ou '(XX) XXXX-XXXX'", regexp = "\\(\\d{2}\\) \\d{4,5}-\\d{4}")
	public String telefone;

	public ClienteDTO() {
		super();
	}

	public ClienteDTO(Cliente c) {
		this.setId(c.id);
		this.setNome(c.nome);
		this.setIdade(c.idade);
		this.setEndereco(c.endereco);
		this.setCpf(c.cpf);
		this.setTelefone(c.telefone);
	}

	public Cliente toEntity() {
		Cliente novo = new Cliente();
		novo.nome = this.getNome();
		novo.idade = this.getIdade();
		novo.endereco = this.getEndereco();
		novo.cpf = this.getCpf();
		novo.telefone = this.getTelefone();
		return novo;
	}

	public long getId() {
		return id;
	}

	public ClienteDTO setId(long id) {
		this.id = id;
		return this;
	}

	public String getNome() {
		return nome;
	}

	public ClienteDTO setNome(String nome) {
		this.nome = nome;
		return this;
	}

	public Integer getIdade() {
		return idade;
	}

	public ClienteDTO setIdade(Integer idade) {
		this.idade = idade;
		return this;
	}

	public String getEndereco() {
		return endereco;
	}

	public ClienteDTO setEndereco(String endereco) {
		this.endereco = endereco;
		return this;
	}

	public String getCpf() {
		return cpf;
	}

	public ClienteDTO setCpf(String cpf) {
		this.cpf = cpf;
		return this;
	}

	public String getTelefone() {
		return telefone;
	}

	public ClienteDTO setTelefone(String telefone) {
		this.telefone = telefone;
		return this;
	}

	public boolean equalsShallow(ClienteDTO cliente) {
		return this.cpf.equals(cliente.getCpf())//
				&& this.endereco.equals(cliente.getEndereco())//
				&& this.id == cliente.getId() //
				&& this.idade == cliente.getIdade()//
				&& this.nome.equals(cliente.getNome())//
				&& this.telefone.equals(cliente.getTelefone());//
	}

}
