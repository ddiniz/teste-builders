package buildersapi.dtos;

import java.util.HashMap;
import java.util.Map;

import javax.validation.constraints.Min;

public class FiltroClienteDTO {
	private Long id;
	private String nome;
	private Integer idade;
	private String endereco;
	private String cpf;
	private String telefone;
	@Min(value = 0, message = "Página minima = 0")
	private int pagina = 0;
	@Min(value = 1, message = "Qtd minima por página  = 1")
	private int qtdPerPage = 1;

	public FiltroClienteDTO() {
		super();
	}

	public String getQuery() {
		StringBuilder sb = new StringBuilder();
		String auxAnd = "";
		if (this.id != null) {
			sb.append(" id = :id ");
			auxAnd = " and ";
		}
		if (this.nome != null && !this.nome.isBlank()) {
			auxAnd = appendAnd(sb, auxAnd);
			sb.append(" nome LIKE :nome ");
		}
		if (this.idade != null) {
			auxAnd = appendAnd(sb, auxAnd);
			sb.append(" idade = :idade ");
		}
		if (this.endereco != null && !this.endereco.isBlank()) {
			auxAnd = appendAnd(sb, auxAnd);
			sb.append(" endereco LIKE :endereco ");
		}
		if (this.cpf != null && !this.cpf.isBlank()) {
			auxAnd = appendAnd(sb, auxAnd);
			sb.append(" cpf LIKE :cpf ");
		}
		if (this.telefone != null && !this.telefone.isBlank()) {
			auxAnd = appendAnd(sb, auxAnd);
			sb.append(" telefone LIKE :telefone ");
		}
		return sb.toString();
	}

	private String appendAnd(StringBuilder sb, String auxAnd) {
		sb.append(auxAnd);
		if (auxAnd.isEmpty())
			auxAnd = " and ";
		return auxAnd;
	}

	public Map<String, Object> getParams() {
		Map<String, Object> params = new HashMap<>();
		if (this.id != null) {
			params.put("id", this.getId());
		}
		if (this.nome != null && !this.nome.isBlank()) {
			params.put("nome", "%" + this.getNome() + "%");
		}
		if (this.idade != null) {
			params.put("idade", this.getIdade());
		}
		if (this.endereco != null && !this.endereco.isBlank()) {
			params.put("endereco", "%" + this.getEndereco() + "%");
		}
		if (this.cpf != null && !this.cpf.isBlank()) {
			params.put("cpf", "%" + this.getCpf() + "%");
		}
		if (this.telefone != null && !this.telefone.isBlank()) {
			params.put("telefone", "%" + this.getTelefone() + "%");
		}
		return params;
	}

	public Long getId() {
		return id;
	}

	public FiltroClienteDTO setId(Long id) {
		this.id = id;
		return this;
	}

	public String getNome() {
		return nome;
	}

	public FiltroClienteDTO setNome(String nome) {
		this.nome = nome;
		return this;
	}

	public Integer getIdade() {
		return idade;
	}

	public FiltroClienteDTO setIdade(Integer idade) {
		this.idade = idade;
		return this;
	}

	public String getEndereco() {
		return endereco;
	}

	public FiltroClienteDTO setEndereco(String endereco) {
		this.endereco = endereco;
		return this;
	}

	public String getCpf() {
		return cpf;
	}

	public FiltroClienteDTO setCpf(String cpf) {
		this.cpf = cpf;
		return this;
	}

	public String getTelefone() {
		return telefone;
	}

	public FiltroClienteDTO setTelefone(String telefone) {
		this.telefone = telefone;
		return this;
	}

	public int getPagina() {
		return pagina;
	}

	public FiltroClienteDTO setPagina(int pagina) {
		this.pagina = pagina;
		return this;
	}

	public int getQtdPerPage() {
		return qtdPerPage;
	}

	public FiltroClienteDTO setQtdPerPage(int qtdPerPage) {
		this.qtdPerPage = qtdPerPage;
		return this;
	}

}
