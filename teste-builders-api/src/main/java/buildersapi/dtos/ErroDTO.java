package buildersapi.dtos;

import javax.ws.rs.core.Response.Status;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ErroDTO {
	private Status status;
	private String msg;
	private String stacktraceMessage;

	public ErroDTO() {
		super();
	}

	public ErroDTO(Status status, String msg, String stacktraceMessage) {
		this.status = status;
		this.msg = msg;
		this.stacktraceMessage = stacktraceMessage;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getStacktraceMessage() {
		return stacktraceMessage;
	}

	public void setStacktraceMessage(String stacktraceMessage) {
		this.stacktraceMessage = stacktraceMessage;
	}

}
