package buildersapi.interfaces.service;

import java.util.List;

import javax.validation.Valid;

import buildersapi.dtos.ClienteDTO;
import buildersapi.dtos.FiltroClienteDTO;
import buildersapi.utils.exceptions.BuildersException;

public interface IClienteService {
	List<ClienteDTO> list(int pagina, int qtdPerPage) throws BuildersException;

	ClienteDTO get(long id) throws BuildersException;

	ClienteDTO save(@Valid ClienteDTO cliente) throws BuildersException;

	ClienteDTO update(@Valid ClienteDTO cliente) throws BuildersException;

	boolean delete(long id) throws BuildersException;

	List<ClienteDTO> filter(FiltroClienteDTO filtroCliente) throws BuildersException;

}
