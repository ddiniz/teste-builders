package buildersapi.interfaces.repository;

import java.util.List;

import javax.validation.Valid;

import buildersapi.dtos.ClienteDTO;
import buildersapi.dtos.FiltroClienteDTO;
import buildersapi.entities.Cliente;
import buildersapi.utils.exceptions.BuildersException;

public interface IClienteRepository {
	List<Cliente> listar(int pagina, int qtdPerPage) throws BuildersException;

	Cliente save(@Valid ClienteDTO cliente) throws BuildersException;

	Cliente update(@Valid ClienteDTO cliente) throws BuildersException;

	boolean delete(long id) throws BuildersException;

	Cliente exists(long id) throws BuildersException;

	List<Cliente> filter(FiltroClienteDTO filtroCliente);
}
