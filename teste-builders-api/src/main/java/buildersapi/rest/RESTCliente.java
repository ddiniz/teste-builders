package buildersapi.rest;

import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import buildersapi.dtos.ClienteDTO;
import buildersapi.dtos.FiltroClienteDTO;
import buildersapi.interfaces.service.IClienteService;
import buildersapi.utils.exceptions.BuildersException;

@Path("/cliente")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class RESTCliente {
    @Inject
    private IClienteService clienteService;

    @GET
    @Path("/list")
    @Tag(name = "Listar clientes", description = "Retorna todos os clientes cadastrados")
    public Response list(@QueryParam("pagina") int pagina, @QueryParam("qtdPerPage") int qtdPerPage)
            throws BuildersException {
        List<ClienteDTO> result = clienteService.list(pagina, qtdPerPage);
        return Response.ok(result).build();
    }

    @POST
    @Path("/filter")
    @Tag(name = "Filtrar clientes", description = "Retorna todos os clientes cadastrados de acordo com o filtro")
    public Response filter(FiltroClienteDTO filtroCliente) throws BuildersException {
        List<ClienteDTO> result = clienteService.filter(filtroCliente);
        return Response.ok(result).build();
    }

    @GET
    @Path("{id}")
    @Tag(name = "Get cliente", description = "Retorna um cliente especifico")
    public Response get(@PathParam("id") final long id) throws BuildersException {
        ClienteDTO result = clienteService.get(id);
        return Response.ok(result).build();
    }

    @POST
    @Tag(name = "Cadastrar cliente", description = "Cadastra um cliente")
    public Response save(@Valid ClienteDTO cliente) throws BuildersException {
        ClienteDTO result = clienteService.save(cliente);
        return Response.ok(result).build();
    }

    @PUT
    @Tag(name = "Atualizar cliente", description = "Atualiza um cliente especifico")
    public Response update(@Valid ClienteDTO cliente) throws BuildersException {
        ClienteDTO result = clienteService.update(cliente);
        return Response.ok(result).build();
    }

    @DELETE
    @Path("{id}")
    @Tag(name = "Deletar cliente", description = "Deleta um cliente especifico")
    public Response delete(@PathParam("id") final long id) throws BuildersException {
        if (clienteService.delete(id))
            return Response.ok().build();
        else
            return Response.serverError().build();
    }
}