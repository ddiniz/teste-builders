package buildersapi.rest.exceptionMapper;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import buildersapi.dtos.ErroDTO;
import buildersapi.utils.StacktraceToString;
import buildersapi.utils.exceptions.BuildersException;

@Provider
public class ExceptionHandler implements ExceptionMapper<BuildersException> {
	@Override
	public Response toResponse(BuildersException e) {
		ErroDTO erro = new ErroDTO(e.getStatus(), e.getMessage(),
				e.isShowStacktrace() ? StacktraceToString.convert(e) : "");
		return Response.status(e.getStatus()).entity(erro).build();
	}
}