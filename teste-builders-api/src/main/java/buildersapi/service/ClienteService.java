package buildersapi.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Response.Status;

import buildersapi.dtos.ClienteDTO;
import buildersapi.dtos.FiltroClienteDTO;
import buildersapi.entities.Cliente;
import buildersapi.interfaces.repository.IClienteRepository;
import buildersapi.interfaces.service.IClienteService;
import buildersapi.utils.exceptions.BuildersException;

@ApplicationScoped
public class ClienteService implements IClienteService {
	@Inject
	IClienteRepository clienteRepository;

	private Cliente exists(long id) throws BuildersException {
		return clienteRepository.exists(id);
	}

	@Override
	public List<ClienteDTO> list(int pagina, int qtdPerPage) throws BuildersException {
		if (pagina < 0 || qtdPerPage < 1)
			throw new BuildersException(
					"Número minimo de páginas deve ser 0 e o número minimo de itens por página deve ser 1.",
					Status.BAD_REQUEST);
		return clienteRepository.listar(pagina, qtdPerPage).stream().map(c -> new ClienteDTO(c))
				.collect(Collectors.toList());
	}

	@Override
	public List<ClienteDTO> filter(FiltroClienteDTO filtroCliente) throws BuildersException {
		return clienteRepository.filter(filtroCliente).stream().map(c -> new ClienteDTO(c))
				.collect(Collectors.toList());
	}

	@Override
	public ClienteDTO get(long id) throws BuildersException {
		Cliente result = exists(id);
		return new ClienteDTO(result);
	}

	@Override
	public ClienteDTO save(ClienteDTO cliente) throws BuildersException {
		return new ClienteDTO(clienteRepository.save(cliente));
	}

	@Override
	public ClienteDTO update(ClienteDTO cliente) throws BuildersException {
		exists(cliente.getId());
		return new ClienteDTO(clienteRepository.update(cliente));
	}

	@Override
	public boolean delete(long id) throws BuildersException {
		exists(id);
		return clienteRepository.delete(id);
	}
}
