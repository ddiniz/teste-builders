package buildersapi.utils.exceptions;

import javax.ws.rs.core.Response.Status;

public class BuildersException extends Exception {

	static final long serialVersionUID = 1L;
	Status status = Status.BAD_REQUEST;
	boolean showStacktrace = true;

	public BuildersException(String msg, Status status) {
		super(msg);
		this.status = status;
		this.showStacktrace = false;
	}

	public BuildersException(String msg, Status status, boolean showStacktrace) {
		super(msg);
		this.status = status;
		this.showStacktrace = showStacktrace;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public boolean isShowStacktrace() {
		return showStacktrace;
	}

	public void setShowStacktrace(boolean showStacktrace) {
		this.showStacktrace = showStacktrace;
	}

}