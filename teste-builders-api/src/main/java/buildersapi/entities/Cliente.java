package buildersapi.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

@Entity
@Table(name = "Cliente")
public class Cliente extends PanacheEntityBase {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;
	public String nome;
	public int idade;
	public String endereco;
	public String cpf;
	public String telefone;

	public boolean equalsShallow(Cliente cliente) {
		return this.cpf.equals(cliente.cpf)//
				&& this.endereco.equals(cliente.endereco)//
				&& this.id == cliente.id //
				&& this.idade == cliente.idade//
				&& this.nome.equals(cliente.nome)//
				&& this.telefone.equals(cliente.telefone);//
	}
}
