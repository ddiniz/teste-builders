package buildersapi.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import buildersapi.dtos.ClienteDTO;
import buildersapi.dtos.FiltroClienteDTO;
import buildersapi.entities.Cliente;
import buildersapi.generators.ClienteDTOGenerator;
import buildersapi.generators.ClienteGenerator;
import buildersapi.interfaces.repository.IClienteRepository;
import buildersapi.interfaces.service.IClienteService;
import buildersapi.utils.exceptions.BuildersException;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;

@QuarkusTest
public class ClienteServiceTest {

	@Inject
	private IClienteService clienteService;
	@InjectMock
	private IClienteRepository clienteRepository;

	@BeforeAll
	public static void setup() {
		MockitoAnnotations.openMocks(ClienteServiceTest.class);
	}

	private List<Cliente> mockListar() {
		return ClienteGenerator.generateValidClientes();
	}

	@Test
	public void testList() throws BuildersException {
		List<Cliente> mock = mockListar();
		Mockito.when(clienteRepository.listar(0, 1)).thenReturn(mock);
		List<ClienteDTO> result = clienteService.list(0, 1);
		assert (result.size() == mock.size());
	}

	@Test
	public void testFilter() throws BuildersException {
		long id = 1L;
		List<Cliente> mock = mockListar().stream().filter(m -> m.id == id).collect(Collectors.toList());
		FiltroClienteDTO filtro = new FiltroClienteDTO().setId(id);
		Mockito.when(clienteRepository.filter(filtro)).thenReturn(mock);
		List<ClienteDTO> result = clienteService.filter(filtro);
		assert (result.size() == mock.size());
	}

	@Test
	public void testGet() throws BuildersException {
		long id = 1L;
		Cliente mock = mockListar().stream().filter(m -> m.id == id).findFirst().get();
		Mockito.when(clienteRepository.exists(id)).thenReturn(mock);
		ClienteDTO result = clienteService.get(id);
		assert (result.id == mock.id);
	}

	@Test
	public void testSave() throws BuildersException {
		ClienteDTO input = ClienteDTOGenerator.generateValidCliente();
		Cliente mock = input.toEntity();
		mock.id = 1L;
		Mockito.when(clienteRepository.save(input)).thenReturn(mock);
		ClienteDTO result = clienteService.save(input);
		input.setId(mock.id);
		assert (result.equalsShallow(input));
	}

	@Test
	public void testUpdate() throws BuildersException {
		ClienteDTO input = ClienteDTOGenerator.generateValidCliente();
		Cliente mock = input.toEntity();
		mock.id = 1L;
		Mockito.when(clienteRepository.update(input)).thenReturn(mock);
		ClienteDTO result = clienteService.update(input);
		input.setId(mock.id);
		assert (result.equalsShallow(input));
	}

	@Test
	public void testDelete() throws BuildersException {
		long id = 1L;
		Mockito.when(clienteRepository.delete(id)).thenReturn(true);
		boolean result = clienteService.delete(id);
		assert (result);
	}
}
