package com.buildersapi.buildersapi.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Cliente")
public class Cliente {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public long id;
	@Column(name = "nome")
	public String nome;
	@Column(name = "idade")
	public int idade;
	@Column(name = "endereco")
	public String endereco;
	@Column(name = "cpf")
	public String cpf;
	@Column(name = "telefone")
	public String telefone;

	public boolean equalsShallow(Cliente cliente) {
		return this.cpf.equals(cliente.cpf)//
				&& this.endereco.equals(cliente.endereco)//
				&& this.id == cliente.id //
				&& this.idade == cliente.idade//
				&& this.nome.equals(cliente.nome)//
				&& this.telefone.equals(cliente.telefone);//
	}

	public Cliente() {
		super();
	}

	public long getId() {
		return id;
	}

	public Cliente setId(long id) {
		this.id = id;
		return this;
	}

	public String getNome() {
		return nome;
	}

	public Cliente setNome(String nome) {
		this.nome = nome;
		return this;
	}

	public int getIdade() {
		return idade;
	}

	public Cliente setIdade(int idade) {
		this.idade = idade;
		return this;
	}

	public String getEndereco() {
		return endereco;
	}

	public Cliente setEndereco(String endereco) {
		this.endereco = endereco;
		return this;
	}

	public String getCpf() {
		return cpf;
	}

	public Cliente setCpf(String cpf) {
		this.cpf = cpf;
		return this;
	}

	public String getTelefone() {
		return telefone;
	}

	public Cliente setTelefone(String telefone) {
		this.telefone = telefone;
		return this;
	}

}
