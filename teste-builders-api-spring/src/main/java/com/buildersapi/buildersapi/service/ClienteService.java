package com.buildersapi.buildersapi.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import com.buildersapi.buildersapi.dtos.ClienteDTO;
import com.buildersapi.buildersapi.dtos.FiltroClienteDTO;
import com.buildersapi.buildersapi.entities.Cliente;
import com.buildersapi.buildersapi.interfaces.repository.IClienteRepository;
import com.buildersapi.buildersapi.interfaces.service.IClienteService;
import com.buildersapi.buildersapi.utils.exceptions.BuildersException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class ClienteService implements IClienteService {
	@Autowired
	private IClienteRepository clienteRepository;

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public ClienteDTO existe(long id) throws BuildersException {
		Cliente result = clienteRepository.findById(id).orElse(null);
		if (result == null)
			throw new BuildersException("Cliente com id (" + id + ") não foi encontrado.", HttpStatus.NOT_FOUND);
		return new ClienteDTO(result);
	}

	@Override
	public List<ClienteDTO> listar(int pagina, int qtdPerPage) throws BuildersException {
		if (pagina < 0 || qtdPerPage < 1)
			throw new BuildersException(
					"Número minimo de páginas deve ser 0 e o número minimo de itens por página deve ser 1.",
					HttpStatus.BAD_REQUEST);
		return clienteRepository.findAll(Pageable.ofSize(qtdPerPage).withPage(pagina)).stream()
				.map(c -> new ClienteDTO(c)).collect(Collectors.toList());
	}

	@Override
	public List<ClienteDTO> filtrar(FiltroClienteDTO filtroCliente) throws BuildersException {
		List<ClienteDTO> result = queryFiltrar(filtroCliente).stream().map(c -> new ClienteDTO(c))
				.collect(Collectors.toList());
		return result;
	}

	private List<Cliente> queryFiltrar(FiltroClienteDTO filtroCliente) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Cliente> query = cb.createQuery(Cliente.class);
		Root<Cliente> cliente = query.from(Cliente.class);
		Path<Long> idPath = cliente.get("id");
		Path<String> nomePath = cliente.get("nome");
		Path<Integer> idadePath = cliente.get("idade");
		Path<String> enderecoPath = cliente.get("endereco");
		Path<String> cpfPath = cliente.get("cpf");
		Path<String> telefonePath = cliente.get("telefone");

		List<Predicate> predicates = new ArrayList<>();
		if (filtroCliente.getId() != null)
			predicates.add(cb.equal(idPath, filtroCliente.getId()));
		if (filtroCliente.getNome() != null)
			predicates.add(cb.like(nomePath, "%" + filtroCliente.getNome() + "%"));
		if (filtroCliente.getIdade() != null)
			predicates.add(cb.equal(idadePath, filtroCliente.getIdade()));
		if (filtroCliente.getEndereco() != null)
			predicates.add(cb.like(enderecoPath, "%" + filtroCliente.getEndereco() + "%"));
		if (filtroCliente.getCpf() != null)
			predicates.add(cb.like(cpfPath, "%" + filtroCliente.getCpf() + "%"));
		if (filtroCliente.getTelefone() != null)
			predicates.add(cb.like(telefonePath, "%" + filtroCliente.getTelefone() + "%"));

		query.select(cliente).where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
		return entityManager.createQuery(query).getResultList();
	}

	@Override

	public ClienteDTO save(ClienteDTO cliente) throws BuildersException {
		return new ClienteDTO(clienteRepository.save(cliente.toEntity()));
	}

	@Override
	@Transactional
	public ClienteDTO atualizar(ClienteDTO cliente) throws BuildersException {
		Cliente clienteAchado = clienteRepository.findById(cliente.getId()).orElse(null);
		clienteAchado.setCpf(cliente.getCpf());
		clienteAchado.setEndereco(cliente.getEndereco());
		clienteAchado.setIdade(cliente.getIdade());
		clienteAchado.setNome(cliente.getNome());
		clienteAchado.setTelefone(cliente.getTelefone());
		return new ClienteDTO(clienteRepository.save(clienteAchado));
	}

	@Override
	@Transactional
	@Modifying
	public boolean deletar(long id) throws BuildersException {
		existe(id);
		clienteRepository.deleteById(id);
		return true;
	}
}
