package com.buildersapi.buildersapi.utils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

public class StacktraceToString {
	public static String convert(Throwable t) {
		Writer writer = new StringWriter();
		t.printStackTrace(new PrintWriter(writer));
		return writer.toString();
	}
}
