package com.buildersapi.buildersapi.utils.exceptions;

import org.springframework.http.HttpStatus;

public class BuildersException extends Exception {

	static final long serialVersionUID = 1L;
	HttpStatus status = HttpStatus.BAD_REQUEST;
	boolean showStacktrace = true;

	public BuildersException(String msg, HttpStatus status) {
		super(msg);
		this.status = status;
		this.showStacktrace = false;
	}

	public BuildersException(String msg, HttpStatus status, boolean showStacktrace) {
		super(msg);
		this.status = status;
		this.showStacktrace = showStacktrace;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	public boolean isShowStacktrace() {
		return showStacktrace;
	}

	public void setShowStacktrace(boolean showStacktrace) {
		this.showStacktrace = showStacktrace;
	}

}