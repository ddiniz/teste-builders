package com.buildersapi.buildersapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BuildersapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BuildersapiApplication.class, args);
	}

}
