package com.buildersapi.buildersapi.interfaces.service;

import java.util.List;

import javax.validation.Valid;

import com.buildersapi.buildersapi.dtos.ClienteDTO;
import com.buildersapi.buildersapi.dtos.FiltroClienteDTO;
import com.buildersapi.buildersapi.utils.exceptions.BuildersException;

public interface IClienteService {
	ClienteDTO existe(long id) throws BuildersException;

	List<ClienteDTO> listar(int pagina, int qtdPerPage) throws BuildersException;

	ClienteDTO save(@Valid ClienteDTO cliente) throws BuildersException;

	ClienteDTO atualizar(@Valid ClienteDTO cliente) throws BuildersException;

	boolean deletar(long id) throws BuildersException;

	List<ClienteDTO> filtrar(FiltroClienteDTO filtroCliente) throws BuildersException;

}
