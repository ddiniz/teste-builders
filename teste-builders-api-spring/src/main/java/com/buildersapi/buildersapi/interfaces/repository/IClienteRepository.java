package com.buildersapi.buildersapi.interfaces.repository;

import com.buildersapi.buildersapi.entities.Cliente;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IClienteRepository extends JpaRepository<Cliente, Long> {
}
