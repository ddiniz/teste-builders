package com.buildersapi.buildersapi.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import org.springframework.http.HttpStatus;

@JsonInclude(Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ErroDTO {
	private HttpStatus status;
	private String msg;
	private String stacktraceMessage;

	public ErroDTO() {
		super();
	}

	public ErroDTO(HttpStatus status, String msg, String stacktraceMessage) {
		this.status = status;
		this.msg = msg;
		this.stacktraceMessage = stacktraceMessage;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getStacktraceMessage() {
		return stacktraceMessage;
	}

	public void setStacktraceMessage(String stacktraceMessage) {
		this.stacktraceMessage = stacktraceMessage;
	}

}
