package com.buildersapi.buildersapi.rest;

import java.util.List;

import javax.validation.Valid;

import com.buildersapi.buildersapi.dtos.ClienteDTO;
import com.buildersapi.buildersapi.dtos.FiltroClienteDTO;
import com.buildersapi.buildersapi.interfaces.service.IClienteService;
import com.buildersapi.buildersapi.utils.exceptions.BuildersException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cliente")
public class RESTCliente extends SuperRest {
    @Autowired
    private IClienteService clienteService;

    @GetMapping(path = "/list", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> list(@RequestParam("pagina") int pagina, @RequestParam("qtdPerPage") int qtdPerPage) {
        try {
            List<ClienteDTO> result = clienteService.listar(pagina, qtdPerPage);
            return ResponseEntity.ok(result);
        } catch (BuildersException e) {
            return erroPadrao(e);
        }
    }

    @PostMapping(path = "/filter", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> filter(@RequestBody FiltroClienteDTO filtroCliente) {
        try {
            List<ClienteDTO> result = clienteService.filtrar(filtroCliente);
            return ResponseEntity.ok(result);
        } catch (BuildersException e) {
            return erroPadrao(e);
        }
    }

    @GetMapping(path = "{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> get(@PathVariable("id") final long id) {
        try {
            ClienteDTO result = clienteService.existe(id);
            return ResponseEntity.ok(result);
        } catch (BuildersException e) {
            return erroPadrao(e);
        }
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> save(@RequestBody @Valid ClienteDTO cliente) {
        try {
            ClienteDTO result = clienteService.save(cliente);
            return ResponseEntity.ok(result);
        } catch (BuildersException e) {
            return erroPadrao(e);
        }
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> update(@RequestBody @Valid ClienteDTO cliente) {
        try {
            ClienteDTO result = clienteService.atualizar(cliente);
            return ResponseEntity.ok(result);
        } catch (BuildersException e) {
            return erroPadrao(e);
        }
    }

    @DeleteMapping(path = "{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> delete(@PathVariable("id") final long id) {
        try {
            if (clienteService.deletar(id))
                return ResponseEntity.ok().build();
            else
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (BuildersException e) {
            return erroPadrao(e);
        }
    }
}