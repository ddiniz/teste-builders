package com.buildersapi.buildersapi.rest;

import com.buildersapi.buildersapi.dtos.ErroDTO;
import com.buildersapi.buildersapi.utils.StacktraceToString;
import com.buildersapi.buildersapi.utils.exceptions.BuildersException;

import org.springframework.http.ResponseEntity;

public class SuperRest {
	protected ResponseEntity<ErroDTO> erroPadrao(BuildersException e) {
		ErroDTO erro = new ErroDTO(e.getStatus(), e.getMessage(),
				e.isShowStacktrace() ? StacktraceToString.convert(e) : "");
		return ResponseEntity.status(e.getStatus()).body(erro);
	}
}
