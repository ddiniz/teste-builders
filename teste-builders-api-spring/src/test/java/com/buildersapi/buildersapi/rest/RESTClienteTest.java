package com.buildersapi.buildersapi.rest;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import com.buildersapi.buildersapi.dtos.ClienteDTO;
import com.buildersapi.buildersapi.dtos.FiltroClienteDTO;
import com.buildersapi.buildersapi.generators.ClienteDTOGenerator;
import com.buildersapi.buildersapi.generators.FiltroClienteDTOGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;

@SpringBootTest
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class RESTClienteTest {

  private static final String PATH = "cliente";
  private static final String PATH_LIST = PATH + "/list?pagina={pagina}&qtdPerPage={qtdPerPage}";
  private static final String PATH_FILTER = PATH + "/filter";
  private static final String PATH_ID = PATH + "/{id}";
  private static final String CONTENT_TYPE = MediaType.APPLICATION_JSON_VALUE;

  private String objToJsonString(Object obj) throws JsonProcessingException {
    ObjectMapper mapper = new ObjectMapper();
    return mapper.writeValueAsString(obj);
  }

  @BeforeEach
  @Transactional
  public void beforeEach() {

  }

  @Test
  @SuppressWarnings("unchecked")
  public void testCaminhoFeliz() throws JsonProcessingException {
    // list inicial
    List<ClienteDTO> list = new ArrayList<>();
    list = given().contentType(CONTENT_TYPE)//
        .when()//
        .get(PATH_LIST, 0, 99)//
        .then()//
        .statusCode(HttpStatus.OK.value())//
        .extract().body().as(list.getClass());
    assertTrue(list.isEmpty(), "Não deveria ter nenhum objeto no banco");

    // save
    ClienteDTO clienteSave = given().contentType(CONTENT_TYPE)//
        .when()//
        .body(objToJsonString(ClienteDTOGenerator.generateValidCliente()))//
        .post(PATH)//
        .then()//
        .statusCode(HttpStatus.OK.value())//
        .extract().body().as(ClienteDTO.class);
    assertTrue(clienteSave != null, "Deveria retornar um objeto não nulo ao salvar");

    // list após save
    list = given().contentType(CONTENT_TYPE)//
        .when()//
        .get(PATH_LIST, 0, 99)//
        .then()//
        .statusCode(HttpStatus.OK.value())//
        .extract().body().as(list.getClass());
    assertTrue(!list.isEmpty(), "Lista de objetos não deveria ser vazia após salvar um objeto");

    // get
    ClienteDTO clienteGet = given().contentType(CONTENT_TYPE)//
        .when()//
        .get(PATH_ID, clienteSave.getId())//
        .then()//
        .statusCode(HttpStatus.OK.value())//
        .extract().body().as(ClienteDTO.class);//
    assertTrue(clienteGet.equalsShallow(clienteSave),
        "O objeto salvo anteriormente deveria ser o mesmo do objeto buscado no banco");

    // update
    clienteGet = ClienteDTOGenerator.updateCliente(clienteGet);
    ClienteDTO clienteUpdate = given().contentType(CONTENT_TYPE)//
        .when()//
        .body(objToJsonString(clienteGet))//
        .put(PATH)//
        .then()//
        .statusCode(HttpStatus.OK.value())//
        .extract().body().as(ClienteDTO.class);
    assertTrue(clienteUpdate.equalsShallow(clienteGet),
        "Objeto atualizado deveria ser o mesmo objeto atualizado no banco");

    // delete
    given().contentType(CONTENT_TYPE)//
        .when()//
        .delete(PATH_ID, clienteUpdate.getId())//
        .then()//
        .statusCode(HttpStatus.OK.value());

    // list após delete
    list = new ArrayList<>();
    list = given().contentType(CONTENT_TYPE)//
        .when()//
        .get(PATH_LIST, 0, 99)//
        .then()//
        .statusCode(HttpStatus.OK.value())//
        .extract().body().as(list.getClass());
    assertTrue(list.isEmpty(), "Não deveria ter nenhum objeto no banco após delete");
  }

  @Test
  @SuppressWarnings("unchecked")
  public void testCaminhoFelizPaginado() throws JsonProcessingException {
    List<ClienteDTO> clientes = ClienteDTOGenerator.generateValidClientes();
    List<ClienteDTO> result = new ArrayList<>();
    for (ClienteDTO dto : clientes) {
      result.add(//
          given().contentType(CONTENT_TYPE)//
              .when()//
              .body(objToJsonString(dto))//
              .post(PATH)//
              .then()//
              .statusCode(HttpStatus.OK.value())//
              .extract().body().as(ClienteDTO.class)//
      );
    }
    List<ClienteDTO> paginated = new ArrayList<>();
    paginated = given().contentType(CONTENT_TYPE)//
        .when()//
        .get(PATH_LIST, 1, 2)//
        .then().statusCode(HttpStatus.OK.value())//
        .extract().body().as(paginated.getClass());
    assertTrue(paginated.size() == 2, "Lista paginada deveria ter só dois elementos por página");
  }

  @Test
  @SuppressWarnings("unchecked")
  public void testCaminhoFelizFiltro() throws JsonProcessingException {
    List<ClienteDTO> clientes = ClienteDTOGenerator.generateValidClientes();
    List<ClienteDTO> result = new ArrayList<>();
    for (ClienteDTO dto : clientes) {
      result.add(//
          given().contentType(CONTENT_TYPE)//
              .when()//
              .body(objToJsonString(dto))//
              .post(PATH)//
              .then()//
              .statusCode(HttpStatus.OK.value())//
              .extract().body().as(ClienteDTO.class)//
      );
    }
    List<FiltroClienteDTO> filtros = FiltroClienteDTOGenerator.generateValidFiltros();
    for (FiltroClienteDTO filtro : filtros) {
      List<ClienteDTO> resultadoFiltrado = new ArrayList<>();
      resultadoFiltrado = given().contentType(CONTENT_TYPE)//
          .when()//
          .body(objToJsonString(filtro))//
          .post(PATH_FILTER)//
          .then().statusCode(HttpStatus.OK.value())//
          .extract().body().as(resultadoFiltrado.getClass());
      assertTrue(!resultadoFiltrado.isEmpty(), "Lista filtrada deveria ter elementos");
    }
  }

  @Test
  public void testGetInvalid() {
    given().contentType(CONTENT_TYPE)//
        .when()//
        .get(PATH_ID, 0)//
        .then().statusCode(HttpStatus.NOT_FOUND.value());//
    given().contentType(CONTENT_TYPE)//
        .when()//
        .get(PATH_ID, 99)//
        .then().statusCode(HttpStatus.NOT_FOUND.value());//
  }

  @Test
  public void testSaveInvalid() {
    for (ClienteDTO invalido : ClienteDTOGenerator.generateInvalidClientes()) {
      given().contentType(CONTENT_TYPE)//
          .when()//
          .body(invalido)//
          .post(PATH)//
          .then()//
          .statusCode(HttpStatus.BAD_REQUEST.value());
    }
  }

  @Test
  public void testUpdateInvalid() throws JsonProcessingException {
    // save
    ClienteDTO clienteSave = given().contentType(CONTENT_TYPE)//
        .when()//
        .body(objToJsonString(ClienteDTOGenerator.generateValidCliente()))//
        .post(PATH)//
        .then()//
        .statusCode(HttpStatus.OK.value())//
        .extract().body().as(ClienteDTO.class);
    assertTrue(clienteSave != null, "Deveria retornar um objeto não nulo ao salvar");

    for (ClienteDTO invalido : ClienteDTOGenerator.generateInvalidClientes()) {
      invalido.setId(clienteSave.getId());
      given().contentType(CONTENT_TYPE)//
          .when()//
          .body(invalido)//
          .put(PATH)//
          .then()//
          .statusCode(HttpStatus.BAD_REQUEST.value());
    }
  }

  @Test
  public void testDeleteInvalid() {
    given().contentType(CONTENT_TYPE)//
        .when()//
        .delete(PATH_ID, 0)//
        .then()//
        .statusCode(HttpStatus.NOT_FOUND.value());
  }

}