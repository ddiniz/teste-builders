package com.buildersapi.buildersapi.generators;

import java.util.ArrayList;
import java.util.List;

import com.buildersapi.buildersapi.entities.Cliente;

public class ClienteGenerator {
	public static List<Cliente> generateValidClientes() {
		List<Cliente> validos = new ArrayList<>();
		validos.add(generateValidCliente(1));
		validos.add(generateValidCliente(2));
		validos.add(generateValidCliente(3));
		validos.add(generateValidCliente(4));
		validos.add(generateValidCliente(5));
		return validos;
	}

	public static Cliente generateValidCliente(long num) {
		Cliente cliente = new Cliente();// cpf invalido
		cliente.id = num;
		cliente.cpf = "344.519.120-40";
		cliente.endereco = "endereco0 valido";
		cliente.idade = 99;
		cliente.nome = "nome1 valido";
		cliente.telefone = "(99) 99999-9999";
		return cliente;
	}
}
