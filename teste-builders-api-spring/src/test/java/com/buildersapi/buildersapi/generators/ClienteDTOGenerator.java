package com.buildersapi.buildersapi.generators;

import java.util.ArrayList;
import java.util.List;

import com.buildersapi.buildersapi.dtos.ClienteDTO;

public class ClienteDTOGenerator {

	public static ClienteDTO generateValidCliente() {
		return new ClienteDTO()//
				.setCpf("344.519.120-40")//
				.setEndereco("endereco valido")//
				.setIdade(99)//
				.setNome("nome valido")//
				.setTelefone("(99) 99999-9999");
	}

	public static ClienteDTO updateCliente(ClienteDTO cliente) {
		return cliente//
				.setCpf("254.395.850-95")//
				.setEndereco("endereco valido update")//
				.setIdade(10)//
				.setNome("nome valido update")//
				.setTelefone("(11) 11111-1111");
	}

	public static List<ClienteDTO> generateInvalidClientes() {
		List<ClienteDTO> invalidos = new ArrayList<>();
		invalidos.add(new ClienteDTO()// cpf invalido
				.setCpf("999.999.999-99")//
				.setEndereco("endereco valido")//
				.setIdade(99)//
				.setNome("nome1 valido")//
				.setTelefone("(99) 99999-9999"));
		invalidos.add(new ClienteDTO()//
				.setCpf("344.519.120-40")//
				.setEndereco("")// endereço invalido
				.setIdade(99)//
				.setNome("nome2 valido")//
				.setTelefone("(99) 99999-9999"));
		invalidos.add(new ClienteDTO()//
				.setCpf("344.519.120-40")//
				.setEndereco("endereco valido")//
				.setIdade(0)// idade invalida
				.setNome("nome3 valido")//
				.setTelefone("(99) 99999-9999"));
		invalidos.add(new ClienteDTO()//
				.setCpf("344.519.120-40")//
				.setEndereco("endereco valido")//
				.setIdade(99)//
				.setNome("")// nome invalido
				.setTelefone("(99) 99999-9999"));
		invalidos.add(new ClienteDTO()//
				.setCpf("344.519.120-40")//
				.setEndereco("endereco valido")//
				.setIdade(99)//
				.setNome("nome valido")//
				.setTelefone(""));// telefone invalido
		return invalidos;
	}

	public static List<ClienteDTO> generateValidClientes() {
		List<ClienteDTO> validos = new ArrayList<>();
		validos.add(new ClienteDTO()// cpf invalido
				.setCpf("344.519.120-40")//
				.setEndereco("endereco0 valido")//
				.setIdade(99)//
				.setNome("nome1 valido")//
				.setTelefone("(99) 99999-9999"));
		validos.add(new ClienteDTO()//
				.setCpf("254.395.850-95")//
				.setEndereco("endereco1 valido")//
				.setIdade(99)//
				.setNome("nome2 valido")//
				.setTelefone("(99) 99999-9999"));
		validos.add(new ClienteDTO()//
				.setCpf("706.361.970-50")//
				.setEndereco("endereco2 valido")//
				.setIdade(99)//
				.setNome("nome3 valido")//
				.setTelefone("(99) 99999-9999"));
		validos.add(new ClienteDTO()//
				.setCpf("140.941.000-59")//
				.setEndereco("endereco3 valido")//
				.setIdade(99)//
				.setNome("nome4 valido")//
				.setTelefone("(99) 99999-9999"));
		validos.add(new ClienteDTO()//
				.setCpf("477.596.450-07")//
				.setEndereco("endereco4 valido")//
				.setIdade(99)//
				.setNome("nome5 valido")//
				.setTelefone("(99) 99999-9999"));
		return validos;
	}
}
