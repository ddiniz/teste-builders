package com.buildersapi.buildersapi.generators;

import java.util.ArrayList;
import java.util.List;

import com.buildersapi.buildersapi.dtos.FiltroClienteDTO;

public class FiltroClienteDTOGenerator {
	public static List<FiltroClienteDTO> generateValidFiltros() {
		List<FiltroClienteDTO> valid = new ArrayList<>();
		valid.add(new FiltroClienteDTO()//
				.setId(2L)//
				.setNome(null)//
				.setCpf(null)//
				.setEndereco(null)//
				.setIdade(null)//
				.setPagina(0)//
				.setTelefone(null)//
				.setQtdPerPage(1)//
		);//
		valid.add(new FiltroClienteDTO()//
				.setId(null)//
				.setNome("nome")//
				.setCpf(null)//
				.setEndereco(null)//
				.setIdade(null)//
				.setPagina(0)//
				.setTelefone(null)//
				.setQtdPerPage(99)//
		);//
		valid.add(new FiltroClienteDTO()//
				.setId(null)//
				.setNome("nome")//
				.setCpf(null)//
				.setEndereco(null)//
				.setIdade(null)//
				.setPagina(0)//
				.setTelefone(null)//
				.setQtdPerPage(99)//
		);//
		valid.add(new FiltroClienteDTO()//
				.setId(null)//
				.setNome(null)//
				.setCpf("344.519.120-40")//
				.setEndereco(null)//
				.setIdade(null)//
				.setPagina(0)//
				.setTelefone(null)//
				.setQtdPerPage(99)//
		);//
		valid.add(new FiltroClienteDTO()//
				.setId(null)//
				.setNome(null)//
				.setCpf(null)//
				.setEndereco("endereco")//
				.setIdade(null)//
				.setPagina(0)//
				.setTelefone(null)//
				.setQtdPerPage(99)//
		);//
		valid.add(new FiltroClienteDTO()//
				.setId(null)//
				.setNome(null)//
				.setCpf(null)//
				.setEndereco(null)//
				.setIdade(99)//
				.setPagina(0)//
				.setTelefone("(99) 99999-9999")//
				.setQtdPerPage(99)//
		);//
		return valid;
	}
}
